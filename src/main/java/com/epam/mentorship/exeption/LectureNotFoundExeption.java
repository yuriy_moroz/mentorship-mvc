package com.epam.mentorship.exeption;

public class LectureNotFoundExeption extends Exception {

    private static final long serialVersionUID = -6153930792868744867L;

    public LectureNotFoundExeption(String id) {
        super("Lecture not found with id "+id);
    }
}
