package com.epam.mentorship.exeption;

public class AssignmentNotFoundExeption extends Exception {

    private static final long serialVersionUID = -3562972787382182679L;

    public AssignmentNotFoundExeption(String id) {
        super("Assignment not found with id "+id);
    }
}
