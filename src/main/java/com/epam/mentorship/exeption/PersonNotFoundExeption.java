package com.epam.mentorship.exeption;

public class PersonNotFoundExeption extends Exception {

    private static final long serialVersionUID = 3420441875794863092L;

    public PersonNotFoundExeption(String id) {
        super("Person not found with id "+id);
    }
}
