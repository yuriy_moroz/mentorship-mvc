package com.epam.mentorship.exeption;

public class GroupNotFoundExeption extends Exception {

    private static final long serialVersionUID = 5976321929086829472L;

    public GroupNotFoundExeption(String id) {
        super("Group not found with id "+id);
    }
}
