package com.epam.mentorship.exeption;

public class ProgramNotFoundExeption extends Exception {

    private static final long serialVersionUID = 1583263089191803365L;

    public ProgramNotFoundExeption(String id) {
        super("Program not found with id "+id);
    }
}
