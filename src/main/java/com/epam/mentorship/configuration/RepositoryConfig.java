package com.epam.mentorship.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.epam.mentorship.dao")
public class RepositoryConfig {
}
