package com.epam.mentorship.controller;

import com.epam.mentorship.exeption.ProgramNotFoundExeption;
import com.epam.mentorship.model.Program;
import com.epam.mentorship.service.ProgramService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/program")
public class ProgramController {

    @Autowired
    private ProgramService programService;

    private static final Logger LOGGER = Logger.getLogger(ProgramController.class);

    @RequestMapping(value = "/add", method={RequestMethod.GET})
    public ModelAndView addProgramPage() {
        ModelAndView modelAndView = new ModelAndView("addProgram");
        modelAndView.addObject("program", new Program());
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String createProgram(@ModelAttribute("program") @Valid Program Program, BindingResult result) {
        if ( result.hasErrors() ) return "redirect:/program/add";
        programService.createProgram(Program);
        return "redirect:/program";
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Program> getProgramById(@PathVariable("id") Long id) {
        Program Program = programService.findProgramById(id);
        if (null == Program) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(Program, HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET,
            produces = {MediaType.TEXT_HTML_VALUE})
    public ModelAndView getProgramByI(@PathVariable("id") Long id) throws ProgramNotFoundExeption {
        ModelAndView modelAndView = new ModelAndView("programDetails");
        Program program = programService.findProgramById(id);
        if ( null == program ) throw new ProgramNotFoundExeption(id.toString());
        modelAndView.addObject("program", program);
        return modelAndView;
    }

    @RequestMapping(value = "{id}", method = RequestMethod.POST)
    public String updateProgram(
            @PathVariable("id") Long id, @ModelAttribute("program") @Valid Program program,
            BindingResult result) throws ProgramNotFoundExeption {
        if ( result.hasErrors() ) return "redirect:/program/"+id;
        Program currentProgram = programService.findProgramById(id);
        if ( null == currentProgram ) throw new ProgramNotFoundExeption(id.toString());
        currentProgram.setName(program.getName());
        currentProgram.setOfficeLocation(program.getOfficeLocation());
        currentProgram.setStartDate(program.getStartDate());
        currentProgram.setEndDate(program.getEndDate());
        programService.updateProgram(currentProgram);
        return "redirect:/program";
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE,
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Program> deleteProgram(@PathVariable("id") Long id) {
        Program Program = programService.findProgramById(id);
        if (null == Program) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        programService.deleteProgram(Program);
        return new ResponseEntity<>(Program, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<Collection<Program>> getAllPrograms() {
        Collection<Program> allPrograms = programService.findAllPrograms();
        return new ResponseEntity<>(allPrograms, HttpStatus.OK);
    }

    @RequestMapping(method={RequestMethod.GET},
            produces = {MediaType.TEXT_HTML_VALUE})
    public ModelAndView getAllProgramsPage() {
        ModelAndView modelAndView = new ModelAndView("programList");
        modelAndView.addObject("programs", programService.findAllPrograms());
        return modelAndView;
    }

    @ExceptionHandler(ProgramNotFoundExeption.class)
    public ModelAndView handleProgramNotFoundException(HttpServletRequest request, Exception ex){
        LOGGER.error("Requested URL="+request.getRequestURL());
        LOGGER.error("Exception Raised="+ex);
        ModelAndView modelAndView = new ModelAndView("error");
        modelAndView.addObject("exception", ex);
        modelAndView.addObject("url", request.getRequestURL());
        return modelAndView;
    }

}
