package com.epam.mentorship.controller;

import com.epam.mentorship.model.Person;
import com.epam.mentorship.exeption.PersonNotFoundExeption;
import com.epam.mentorship.service.PersonService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/person")
public class PersonController {

    private static final Logger LOGGER = Logger.getLogger(PersonController.class);

    @Autowired
    private PersonService personService;

    @RequestMapping(value = "/add", method={RequestMethod.GET})
    public ModelAndView addPersonPage() {
        ModelAndView modelAndView = new ModelAndView("addPerson");
        modelAndView.addObject("person", new Person());
        modelAndView.addObject("managers", personService.findAllPersons());
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String createPerson(@ModelAttribute("person") @Valid Person person, BindingResult result) {

        if ( result.hasErrors() ) return "redirect:/person/add";
        personService.createPerson(person);
        return "redirect:/person";
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Person> getPersonById(@PathVariable("id") Long id) {
        Person person = personService.findPersonById(id);
        if (null == person) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(person, HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET,
                    produces = {MediaType.TEXT_HTML_VALUE})
    public ModelAndView getPersonDetails(@PathVariable("id") Long id) throws PersonNotFoundExeption {
        ModelAndView modelAndView = new ModelAndView("personDetails");
        Person person = personService.findPersonById(id);
        if ( null == person ) throw new PersonNotFoundExeption(id.toString());
        Person manager = personService.findPersonById(person.getManager());
        modelAndView.addObject("person", person);
        modelAndView.addObject("personManager", manager);
        modelAndView.addObject("managers", personService.findAllPersons());
        return modelAndView;
    }

    @RequestMapping(value = "{id}", method = RequestMethod.POST)
    public String updatePerson(
            @PathVariable("id") Long id, @ModelAttribute("person") @Valid Person person,
            BindingResult result) throws PersonNotFoundExeption {
        if ( result.hasErrors() ) return "redirect:/person/"+id;
        Person currentPerson = personService.findPersonById(id);
        if ( null == currentPerson ) throw new PersonNotFoundExeption(id.toString());
        currentPerson.setBirthDate(person.getBirthDate());
        currentPerson.setEmail(person.getEmail());
        currentPerson.setLevel(person.getLevel());
        currentPerson.setManager(person.getManager());
        currentPerson.setName(person.getName());
        currentPerson.setPrimarySkill(person.getPrimarySkill());
        personService.updatePerson(currentPerson);
        return "redirect:/person/";
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE,
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Person> deletePerson(@PathVariable("id") Long id) {
        Person person = personService.findPersonById(id);
        if (null == person) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        personService.deletePerson(person);
        return new ResponseEntity<>(person, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<Collection<Person>> getAllPersons() {
        Collection<Person> allPersons = personService.findAllPersons();
        return new ResponseEntity<>(allPersons, HttpStatus.OK);
    }

    @RequestMapping(method={RequestMethod.GET},
            produces = {MediaType.TEXT_HTML_VALUE})
    public ModelAndView getAllPersonsPage(
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "primarySkill", required = false) String primarySkill) {
        ModelAndView modelAndView = new ModelAndView("personList");
        if ((!StringUtils.isEmpty(name)) || (!StringUtils.isEmpty(primarySkill))) {
            modelAndView.addObject("persons", personService.findPersonByNameAndPrimarySkill(name, primarySkill));
        } else {
            modelAndView.addObject("persons", personService.findAllPersons());
        }
        return modelAndView;
    }

    @ExceptionHandler(PersonNotFoundExeption.class)
    public ModelAndView handlePersonNotFoundException(HttpServletRequest request, Exception ex){
        LOGGER.error("Requested URL="+request.getRequestURL());
        LOGGER.error("Exception Raised="+ex);
        ModelAndView modelAndView = new ModelAndView("error");
        modelAndView.addObject("exception", ex);
        modelAndView.addObject("url", request.getRequestURL());
        return modelAndView;
    }

}
