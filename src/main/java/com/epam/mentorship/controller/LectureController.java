package com.epam.mentorship.controller;

import com.epam.mentorship.model.Lecture;
import com.epam.mentorship.model.Person;
import com.epam.mentorship.exeption.LectureNotFoundExeption;
import com.epam.mentorship.service.LectureService;
import com.epam.mentorship.service.PersonService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/lecture")
public class LectureController {

    private static final Logger LOGGER = Logger.getLogger(LectureController.class);

    @Autowired
    private PersonService personService;
    @Autowired
    private LectureService lectureService;

    @RequestMapping(value = "/add", method={RequestMethod.GET})
    public ModelAndView addLecturePage() {
        ModelAndView modelAndView = new ModelAndView("addLecture");
        modelAndView.addObject("lecture", new Lecture());
        modelAndView.addObject("lectors", personService.findAllPersons());
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String createLecture(@ModelAttribute("lecture") @Valid Lecture lecture, BindingResult result) {

        if ( result.hasErrors() ) return "redirect:/lecture/add";
        lectureService.createLecture(lecture);
        return "redirect:/lecture";
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Lecture> getLectureById(@PathVariable("id") Long id) {
        Lecture lecture = lectureService.findLectureById(id);
        if (null == lecture) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(lecture, HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET,
            produces = {MediaType.TEXT_HTML_VALUE})
    public ModelAndView getLectureDetails(@PathVariable("id") Long id) throws LectureNotFoundExeption {
        ModelAndView modelAndView = new ModelAndView("lectureDetails");
        Lecture lecture = lectureService.findLectureById(id);
        if ( null == lecture ) throw new LectureNotFoundExeption(id.toString());
        modelAndView.addObject("lecture", lecture);
        Person lector = personService.findPersonById(lecture.getLector());
        modelAndView.addObject("currentLector", lector);
        modelAndView.addObject("lectors", personService.findAllPersons());
        return modelAndView;
    }

    @RequestMapping(value = "{id}", method = RequestMethod.POST)
    public String updateLecture(
            @PathVariable("id") Long id, @ModelAttribute("lecture") @Valid Lecture lecture,
            BindingResult result) throws LectureNotFoundExeption {
        if ( result.hasErrors() ) return "redirect:/lecture/"+id;
        Lecture currentLecture = lectureService.findLectureById(id);
        if ( null == currentLecture ) throw new LectureNotFoundExeption(id.toString());
        currentLecture.setTopic(lecture.getTopic());
        currentLecture.setStatus(lecture.getStatus());
        currentLecture.setLector(lecture.getLector());
        currentLecture.setDomainArea(lecture.getDomainArea());
        currentLecture.setDuration(lecture.getDuration());
        currentLecture.setScheduledTime(lecture.getScheduledTime());
        lectureService.updateLecture(currentLecture);
        return "redirect:/lecture/";
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE,
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Lecture> deleteLecture(@PathVariable("id") Long id) {
        Lecture lecture = lectureService.findLectureById(id);
        if (null == lecture) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        lectureService.deleteLecture(lecture);
        return new ResponseEntity<>(lecture, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<Collection<Lecture>> getAlllectures() {
        Collection<Lecture> allLectures = lectureService.findAllLectures();
        return new ResponseEntity<>(allLectures, HttpStatus.OK);
    }

    @RequestMapping(method={RequestMethod.GET},
            produces = {MediaType.TEXT_HTML_VALUE})
    public ModelAndView getAllLecturesPage() {
        ModelAndView modelAndView = new ModelAndView("lectureList");
        modelAndView.addObject("lectures", lectureService.findAllLectures());
        return modelAndView;
    }

    @ExceptionHandler(LectureNotFoundExeption.class)
    public ModelAndView handleLectureNotFoundException(HttpServletRequest request, Exception ex){
        LOGGER.error("Requested URL="+request.getRequestURL());
        LOGGER.error("Exception Raised="+ex);
        ModelAndView modelAndView = new ModelAndView("error");
        modelAndView.addObject("exception", ex);
        modelAndView.addObject("url", request.getRequestURL());
        return modelAndView;
    }
}
