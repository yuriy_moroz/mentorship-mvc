package com.epam.mentorship.controller;

import com.epam.mentorship.exeption.GroupNotFoundExeption;
import com.epam.mentorship.model.Group;
import com.epam.mentorship.model.Person;
import com.epam.mentorship.model.wrapper.GroupWrapper;
import com.epam.mentorship.service.GroupService;
import com.epam.mentorship.service.PersonService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Controller
@RequestMapping("/group")
public class GroupController {

    private static final Logger LOGGER = Logger.getLogger(GroupController.class);
    @Autowired
    private GroupService groupService;
    @Autowired
    private PersonService personService;

    @RequestMapping(value = "/add", method = {RequestMethod.GET})
    public ModelAndView addGroupPage() {
        ModelAndView modelAndView = new ModelAndView("addGroup");
        modelAndView.addObject("group", new Group());
        modelAndView.addObject("persons", personService.findAllPersons());
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String createGroup(@ModelAttribute("group") @Valid Group group, BindingResult result) {
        if (result.hasErrors()) {
            return "redirect:/group/add";
        }
        groupService.createGroup(group);
        return "redirect:/group";
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Group> getGroupById(@PathVariable("id") Long id) {
        Group group = groupService.findGroupById(id);
        if (null == group) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(group, HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET,
            produces = {MediaType.TEXT_HTML_VALUE})
    public ModelAndView getGroupDetails(@PathVariable("id") Long id) throws GroupNotFoundExeption {
        ModelAndView modelAndView = new ModelAndView("groupDetails");
        Group group = groupService.findGroupById(id);
        if (null == group) {
            throw new GroupNotFoundExeption(id.toString());
        }
        Person mentor = personService.findPersonById(group.getMentor());
        Person mentee = personService.findPersonById(group.getMentee());
        modelAndView.addObject("group", group);
        modelAndView.addObject("mentor", mentor);
        modelAndView.addObject("mentee", mentee);
        return modelAndView;
    }

    @RequestMapping(value = "{id}", method = RequestMethod.POST)
    public String updateGroup(
            @PathVariable("id") Long id, @ModelAttribute("group") @Valid Group group,
            BindingResult result) throws GroupNotFoundExeption {
        if (result.hasErrors()) {
            return "redirect:/group/" + id;
        }
        Group currentGroup = groupService.findGroupById(id);
        if (null == currentGroup) throw new GroupNotFoundExeption(id.toString());
        currentGroup.setMentor(group.getMentor());
        currentGroup.setMentee(group.getMentee());
        currentGroup.setStatus(group.getStatus());
        currentGroup.setPlannedStart(group.getPlannedStart());
        currentGroup.setPlannedEnd(group.getPlannedEnd());
        currentGroup.setActualEnd(group.getActualStart());
        currentGroup.setActualEnd(group.getActualEnd());
        groupService.updateGroup(currentGroup);
        return "redirect:/group/";
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE,
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Group> deleteGroup(@PathVariable("id") Long id) {
        Group group = groupService.findGroupById(id);
        if (null == group) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        groupService.deleteGroup(group);
        return new ResponseEntity<>(group, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<Collection<Group>> getAllGroups() {
        Collection<Group> allGroups = groupService.findAllGroups();
        return new ResponseEntity<>(allGroups, HttpStatus.OK);
    }

    @RequestMapping(method = {RequestMethod.GET},
            produces = {MediaType.TEXT_HTML_VALUE})
    public ModelAndView getAllGroupsPage() {
        ModelAndView modelAndView = new ModelAndView("groupList");
        modelAndView.addObject("groups", populateGroups(groupService.findAllGroups()));
        return modelAndView;
    }

    private List<GroupWrapper> populateGroups(Collection<Group> allGroups) {
        List<GroupWrapper> groupWrappers = new ArrayList<>(allGroups.size());
        for ( Group group : allGroups ) {
            Person mentor = personService.findPersonById(group.getMentor());
            Person mentee = personService.findPersonById(group.getMentee());
            groupWrappers.add(new GroupWrapper(group, mentor.getName(), mentee.getName()));
        }
        return groupWrappers;
    }

    @ExceptionHandler(GroupNotFoundExeption.class)
    public ModelAndView handleGroupNotFoundException(HttpServletRequest request, Exception ex) {
        LOGGER.error("Requested URL=" + request.getRequestURL());
        LOGGER.error("Exception Raised=" + ex);
        ModelAndView modelAndView = new ModelAndView("error");
        modelAndView.addObject("exception", ex);
        modelAndView.addObject("url", request.getRequestURL());
        return modelAndView;
    }

}
