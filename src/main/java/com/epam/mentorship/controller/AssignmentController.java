package com.epam.mentorship.controller;

import com.epam.mentorship.model.Assignment;
import com.epam.mentorship.model.Person;
import com.epam.mentorship.exeption.AssignmentNotFoundExeption;
import com.epam.mentorship.model.wrapper.AssignmentWrapper;
import com.epam.mentorship.service.AssignmentService;
import com.epam.mentorship.service.PersonService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Controller
@RequestMapping("/assignment")
public class AssignmentController {

    private static final Logger LOGGER = Logger.getLogger(AssignmentController.class);
    @Autowired
    private PersonService personService;
    @Autowired
    private AssignmentService assignmentService;

    @RequestMapping(value = "/add", method={RequestMethod.GET})
    public ModelAndView addAssignmentPage() {
        ModelAndView modelAndView = new ModelAndView("addAssignment");
        modelAndView.addObject("assignment", new Assignment());
        modelAndView.addObject("persons", personService.findAllPersons());
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String createAssignment(
            @ModelAttribute("assignment") @Valid Assignment assignment, BindingResult result) {
        if ( result.hasErrors() ) {
            return "redirect:/assignment/add";
        }
        assignmentService.createAssignment(assignment);
        return "redirect:/assignment";
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Assignment> getAssignmentById(@PathVariable("id") Long id) {
        Assignment assignment = assignmentService.findAssignmentById(id);
        if (null == assignment) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(assignment, HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET,
            produces = {MediaType.TEXT_HTML_VALUE})
    public ModelAndView getAssignmentDetails(
            @PathVariable("id") Long id) throws AssignmentNotFoundExeption {
        ModelAndView modelAndView = new ModelAndView("assignmentDetails");
        Assignment assignment = assignmentService.findAssignmentById(id);
        if ( null == assignment ) throw new AssignmentNotFoundExeption(id.toString());
        Person person = personService.findPersonById(assignment.getId());
        modelAndView.addObject("assignment", new AssignmentWrapper(assignment, person.getName()));
        modelAndView.addObject("persons", personService.findAllPersons());
        return modelAndView;
    }

    @RequestMapping(value = "{id}", method = RequestMethod.POST)
    public String updateAssignment(
            @PathVariable("id") Long id, @ModelAttribute("assignment") @Valid Assignment assignment,
            BindingResult result) throws AssignmentNotFoundExeption {
        if ( result.hasErrors() ) {
            return "redirect:/assignment/"+id;
        }
        Assignment currentAssignment = assignmentService.findAssignmentById(id);
        if ( null == currentAssignment ) {
            throw new AssignmentNotFoundExeption(id.toString());
        }
        currentAssignment.setPerson(assignment.getPerson());
        currentAssignment.setRole(assignment.getRole());
        currentAssignment.setStatus(assignment.getStatus());
        return "redirect:/assignment/";
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE,
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Assignment> deleteAssignment(@PathVariable("id") Long id) {
        Assignment assignment = assignmentService.findAssignmentById(id);
        if (null == assignment) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        assignmentService.deleteAssignment(assignment);
        return new ResponseEntity<>(assignment, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<Collection<Assignment>> getAllAssignments() {
        Collection<Assignment> allAssignments = assignmentService.findAllAssignments();
        return new ResponseEntity<>(allAssignments, HttpStatus.OK);
    }

    @RequestMapping(method={RequestMethod.GET},
            produces = {MediaType.TEXT_HTML_VALUE})
    public ModelAndView getAllAssignmentsPage() {
        ModelAndView modelAndView = new ModelAndView("assignmentList");
        modelAndView.addObject("assignments", populateAssignments(assignmentService.findAllAssignments()));
        return modelAndView;
    }

    private List<AssignmentWrapper> populateAssignments(Collection<Assignment> allAssignments) {
        List<AssignmentWrapper> assignmentWrappers = new ArrayList<>(allAssignments.size());
        for ( Assignment assignment : allAssignments ) {
            Person person = personService.findPersonById(assignment.getPerson());
            AssignmentWrapper assignmentWrapper = new AssignmentWrapper(assignment, person.getName());
            assignmentWrappers.add(assignmentWrapper);
        }
        return assignmentWrappers;
    }

    @ExceptionHandler(AssignmentNotFoundExeption.class)
    public ModelAndView handleAssignmentNotFoundException(HttpServletRequest request, Exception ex){
        LOGGER.error("Requested URL="+request.getRequestURL());
        LOGGER.error("Exception Raised="+ex);
        ModelAndView modelAndView = new ModelAndView("error");
        modelAndView.addObject("exception", ex);
        modelAndView.addObject("url", request.getRequestURL());
        return modelAndView;
    }

}
