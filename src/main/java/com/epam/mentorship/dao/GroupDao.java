package com.epam.mentorship.dao;

import com.epam.mentorship.model.Group;

public interface GroupDao extends AbstractDao<Group> {
}
