package com.epam.mentorship.dao;

import com.epam.mentorship.model.Program;

public interface ProgramDao extends AbstractDao<Program> {
}