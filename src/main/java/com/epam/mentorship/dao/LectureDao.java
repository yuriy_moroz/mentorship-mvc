package com.epam.mentorship.dao;

import com.epam.mentorship.model.Lecture;

public interface LectureDao extends AbstractDao<Lecture> {
}
