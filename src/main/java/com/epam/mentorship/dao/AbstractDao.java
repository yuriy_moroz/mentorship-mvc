package com.epam.mentorship.dao;

import com.epam.mentorship.model.AbstractModel;

import java.util.Collection;
import java.util.List;

public interface AbstractDao<M extends AbstractModel> {
    M create(M entity);
    M update(M entity);
    void delete(M entity);
    Collection<M> getAll();
    M findById(Long id);
}
