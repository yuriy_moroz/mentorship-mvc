package com.epam.mentorship.dao;

import com.epam.mentorship.model.Assignment;

public interface AssignmentDao extends AbstractDao<Assignment> {
}
