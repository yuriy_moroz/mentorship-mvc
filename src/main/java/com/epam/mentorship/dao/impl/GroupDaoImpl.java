package com.epam.mentorship.dao.impl;

import com.epam.mentorship.dao.GroupDao;
import com.epam.mentorship.model.Group;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Repository
public class GroupDaoImpl implements GroupDao {

    private AtomicLong counter = new AtomicLong(0);
    private Map<Long, Group> groupMap = new ConcurrentHashMap<>();

    @Override
    public Group create(Group entity) {
        Long id = counter.incrementAndGet();
        entity.setId(id);
        groupMap.put(id, entity);
        return entity;
    }

    @Override
    public Group update(Group entity) {
        groupMap.put(entity.getId(), entity);
        return entity;
    }

    @Override
    public void delete(Group entity) {
        groupMap.remove(entity.getId());
    }

    @Override
    public Collection<Group> getAll() {
        return groupMap.values();
    }

    @Override
    public Group findById(Long id) {
        return groupMap.get(id);
    }

}
