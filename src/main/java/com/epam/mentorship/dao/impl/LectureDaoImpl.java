package com.epam.mentorship.dao.impl;

import com.epam.mentorship.dao.LectureDao;
import com.epam.mentorship.model.Lecture;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Repository
public class LectureDaoImpl implements LectureDao {

    private AtomicLong counter = new AtomicLong(0);
    private Map<Long, Lecture> lectureMap = new ConcurrentHashMap<>();

    @Override
    public Lecture create(Lecture entity) {
        Long id = counter.incrementAndGet();
        entity.setId(id);
        lectureMap.put(id, entity);
        return entity;
    }

    @Override
    public Lecture update(Lecture entity) {
        lectureMap.put(entity.getId(), entity);
        return entity;
    }

    @Override
    public void delete(Lecture entity) {
        lectureMap.remove(entity.getId());
    }

    @Override
    public Collection<Lecture> getAll() {
        return lectureMap.values();
    }

    @Override
    public Lecture findById(Long id) {
        return lectureMap.get(id);
    }

}
