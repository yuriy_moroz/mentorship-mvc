package com.epam.mentorship.dao.impl;

import com.epam.mentorship.dao.PersonDao;
import com.epam.mentorship.model.Person;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Repository
public class PersonDaoImpl implements PersonDao {

    private AtomicLong counter = new AtomicLong(0);
    private Map<Long, Person> personMap = new ConcurrentHashMap<Long, Person>() {{
        Long id = counter.incrementAndGet();
        put(id, new Person(id,
                "CEO", "RootManager@email.com", Person.Level.L5, "C++", null, new Date()));
        id = counter.incrementAndGet();
        put(id, new Person(id,
                "Manager1", "Manager1@email.com", Person.Level.L4, "Java", 1L, new Date()));
        id = counter.incrementAndGet();
        put(id, new Person(id,
                "Manager2", "Manager2@email.com", Person.Level.L4, "Ruby", 1L, new Date()));
        id = counter.incrementAndGet();
        put(id, new Person(id,
                "Manager3", "Manager3@email.com", Person.Level.L4, "Python", 1L, new Date()));
        id = counter.incrementAndGet();
        put(id, new Person(id,
                "Manager4", "Manager4@email.com", Person.Level.L4, "C#", 1L, new Date()));
    }};

    @Override
    public Person create(Person entity) {
        Long id = counter.incrementAndGet();
        entity.setId(id);
        personMap.put(id, entity);
        return entity;
    }

    @Override
    public Person update(Person entity) {
        personMap.put(entity.getId(), entity);
        return entity;
    }

    @Override
    public void delete(Person entity) {
        personMap.remove(entity.getId());
    }

    @Override
    public Collection<Person> getAll() {
        return personMap.values();
    }

    @Override
    public Person findById(Long id) {
        return personMap.get(id);
    }

    @Override
    public List<Person> findPersonByNameAndPrimarySkill(String name, String primarySkill) {

        List<Person> result = new ArrayList<>();
        for (Person person : personMap.values()) {
            if (person.getName().equals(name) && person.getPrimarySkill().equals(primarySkill)) {
                result.add(person);
                continue;
            }

            if (!StringUtils.isEmpty(name)) {
                if (name.equals(person.getName())) {
                    result.add(person);
                }
            }

            if (!StringUtils.isEmpty(primarySkill)) {
                if (primarySkill.equals(person.getPrimarySkill())) {
                    result.add(person);
                }
            }
        }
        return result;
    }
}
