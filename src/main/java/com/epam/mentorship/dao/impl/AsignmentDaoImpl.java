package com.epam.mentorship.dao.impl;

import com.epam.mentorship.dao.AssignmentDao;
import com.epam.mentorship.model.Assignment;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Repository
public class AsignmentDaoImpl implements AssignmentDao {

    private AtomicLong counter = new AtomicLong(0);
    private Map<Long, Assignment> assignmentMap = new ConcurrentHashMap<>();

    @Override
    public Assignment create(Assignment entity) {
        Long id = counter.incrementAndGet();
        entity.setId(id);
        assignmentMap.put(id, entity);
        return entity;
    }

    @Override
    public Assignment update(Assignment entity) {
        assignmentMap.put(entity.getId(), entity);
        return entity;
    }

    @Override
    public void delete(Assignment entity) {
        assignmentMap.remove(entity.getId());
    }

    @Override
    public Collection<Assignment> getAll() {
        return assignmentMap.values();
    }

    @Override
    public Assignment findById(Long id) {
        return assignmentMap.get(id);
    }
}
