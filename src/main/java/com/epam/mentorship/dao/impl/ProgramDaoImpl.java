package com.epam.mentorship.dao.impl;

import com.epam.mentorship.dao.ProgramDao;
import com.epam.mentorship.model.Program;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Repository
public class ProgramDaoImpl implements ProgramDao {

    private AtomicLong counter = new AtomicLong(0);
    private Map<Long, Program> programMap = new ConcurrentHashMap<Long, Program>(){{
        Long id = counter.incrementAndGet();
        put(id, new Program(id, "Program 1", "Lviv", new Date(1472688000000L), new Date(1477958400000L)));
        id = counter.incrementAndGet();
        put(id, new Program(id, "Program 2", "Kiev", new Date(1475280000000L), new Date(1480550400000L)));
    }};

    @Override
    public Program create(Program entity) {
        Long id = counter.incrementAndGet();
        entity.setId(id);
        programMap.put(id, entity);
        return entity;
    }

    @Override
    public Program update(Program entity) {
        programMap.put(entity.getId(), entity);
        return entity;
    }

    @Override
    public void delete(Program entity) {
        programMap.remove(entity.getId());
    }

    @Override
    public Collection<Program> getAll() {
        return programMap.values();
    }

    @Override
    public Program findById(Long id) {
        return programMap.get(id);
    }
}
