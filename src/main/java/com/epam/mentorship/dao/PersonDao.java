package com.epam.mentorship.dao;

import com.epam.mentorship.model.Person;

import java.util.List;

public interface PersonDao extends AbstractDao<Person> {

    List<Person> findPersonByNameAndPrimarySkill(String name, String primarySkill);
}
