package com.epam.mentorship.service.impl;

import com.epam.mentorship.dao.PersonDao;
import com.epam.mentorship.model.Person;
import com.epam.mentorship.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service("personService")
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonDao personDao;

    @Override
    public Person createPerson(Person person) {
        return personDao.create(person);
    }

    @Override
    public Collection<Person> findAllPersons() {
        return personDao.getAll();
    }

    @Override
    public Person findPersonById(Long id) {
        if ( null == id ) return null;
        return personDao.findById(id);
    }

    @Override
    public void deletePerson(Person person) {
        personDao.delete(person);
    }

    @Override
    public Person updatePerson(Person person) {
        return personDao.update(person);
    }

    @Override
    public List<Person> findPersonByNameAndPrimarySkill(String name, String primarySkill) {
        return personDao.findPersonByNameAndPrimarySkill(name, primarySkill);
    }
}
