package com.epam.mentorship.service.impl;

import com.epam.mentorship.dao.GroupDao;
import com.epam.mentorship.model.Group;
import com.epam.mentorship.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service("groupService")
public class GroupServiceImpl implements GroupService {

    @Autowired
    private GroupDao groupDao;

    @Override
    public Group createGroup(Group group) {
        return groupDao.create(group);
    }

    @Override
    public Collection<Group> findAllGroups() {
        return groupDao.getAll();
    }

    @Override
    public Group findGroupById(Long id) {
        return groupDao.findById(id);
    }

    @Override
    public void deleteGroup(Group group) {
        groupDao.delete(group);
    }

    @Override
    public Group updateGroup(Group group) {
        return groupDao.update(group);
    }
}
