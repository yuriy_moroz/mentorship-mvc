package com.epam.mentorship.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.mentorship.dao.ProgramDao;
import com.epam.mentorship.model.Program;
import com.epam.mentorship.service.ProgramService;

import java.util.Collection;

@Service("programService")
public class ProgramServiceImpl implements ProgramService{

    @Autowired
    private ProgramDao programDao;

    @Override
    public Program createProgram(Program person) {
        return programDao.create(person);
    }

    @Override
    public Collection<Program> findAllPrograms() {
        return programDao.getAll();
    }

    @Override
    public Program findProgramById(Long id) {
        return programDao.findById(id);
    }

    @Override
    public void deleteProgram(Program person) {
        programDao.delete(person);
    }

    @Override
    public Program updateProgram(Program person) {
        return programDao.update(person);
    }
}
