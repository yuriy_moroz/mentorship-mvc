package com.epam.mentorship.service.impl;

import com.epam.mentorship.dao.AssignmentDao;
import com.epam.mentorship.model.Assignment;
import com.epam.mentorship.service.AssignmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service("assignmentService")
public class AssignmentServiceImpl implements AssignmentService {

    @Autowired
    private AssignmentDao assignmentDao;

    @Override
    public Assignment createAssignment(Assignment assignment) {
        return assignmentDao.create(assignment);
    }

    @Override
    public Collection<Assignment> findAllAssignments() {
        return assignmentDao.getAll();
    }

    @Override
    public Assignment findAssignmentById(Long id) {
        return assignmentDao.findById(id);
    }

    @Override
    public void deleteAssignment(Assignment assignment) {
        assignmentDao.delete(assignment);
    }

    @Override
    public Assignment updateAssignment(Assignment assignment) {
        return assignmentDao.update(assignment);
    }
}
