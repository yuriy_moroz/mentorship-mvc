package com.epam.mentorship.service.impl;

import com.epam.mentorship.dao.LectureDao;
import com.epam.mentorship.model.Lecture;
import com.epam.mentorship.service.LectureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service("lectureService")
public class LectureServiceImpl implements LectureService {

    @Autowired
    private LectureDao lectureDao;

    @Override
    public Lecture createLecture(Lecture lecture) {
        return lectureDao.create(lecture);
    }

    @Override
    public Collection<Lecture> findAllLectures() {
        return lectureDao.getAll();
    }

    @Override
    public Lecture findLectureById(Long id) {
        return lectureDao.findById(id);
    }

    @Override
    public void deleteLecture(Lecture lecture) {
        lectureDao.delete(lecture);
    }

    @Override
    public Lecture updateLecture(Lecture lecture) {
        return lectureDao.update(lecture);
    }
}
