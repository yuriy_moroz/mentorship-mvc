package com.epam.mentorship.service;

import com.epam.mentorship.model.Program;

import java.util.Collection;

public interface ProgramService {
    Program createProgram(Program person);
    Collection<Program> findAllPrograms();
    Program findProgramById(Long id);
    void deleteProgram(Program person);
    Program updateProgram(Program person);

}
