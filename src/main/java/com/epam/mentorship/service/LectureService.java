package com.epam.mentorship.service;

import com.epam.mentorship.model.Lecture;

import java.util.Collection;

public interface LectureService {
    Lecture createLecture(Lecture lecture);
    Collection<Lecture> findAllLectures();
    Lecture findLectureById(Long id);
    void deleteLecture(Lecture lecture);
    Lecture updateLecture(Lecture lecture);

}
