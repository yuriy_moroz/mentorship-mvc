package com.epam.mentorship.service;

import com.epam.mentorship.model.Group;

import java.util.Collection;

public interface GroupService {
    Group createGroup(Group group);
    Collection<Group> findAllGroups();
    Group findGroupById(Long id);
    void deleteGroup(Group group);
    Group updateGroup(Group group);

}
