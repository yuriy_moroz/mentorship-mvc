package com.epam.mentorship.service;

import com.epam.mentorship.model.Person;

import java.util.Collection;
import java.util.List;

public interface PersonService {
    Person createPerson(Person person);
    Collection<Person> findAllPersons();
    Person findPersonById(Long id);
    void deletePerson(Person person);
    Person updatePerson(Person person);
    List<Person> findPersonByNameAndPrimarySkill(String name, String primarySkill);

}
