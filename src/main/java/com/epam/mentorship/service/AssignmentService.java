package com.epam.mentorship.service;

import com.epam.mentorship.model.Assignment;

import java.util.Collection;

public interface AssignmentService {
    Assignment createAssignment(Assignment assignment);
    Collection<Assignment> findAllAssignments();
    Assignment findAssignmentById(Long id);
    void deleteAssignment(Assignment assignment);
    Assignment updateAssignment(Assignment assignment);

}
