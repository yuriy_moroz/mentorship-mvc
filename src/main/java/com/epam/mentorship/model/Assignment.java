package com.epam.mentorship.model;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class Assignment extends AbstractModel implements Serializable {

    private static final long serialVersionUID = -1788066901943975508L;

    @NotNull
    private Long person;

    @NotNull
    private Role role;

    @NotNull
    private Status status;

    public enum Role {
        MENTOR,
        MENTEE,
        CURATOR,
        LECTOR
    }

    public enum Status {
        PROPOSED,
        APPROVED_RM,
        CONFIRMED_CDP,
        ON_HOLD
    }

    public Long getPerson() {
        return person;
    }

    public void setPerson(Long person) {
        this.person = person;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Assignment{" +
                "person=" + person +
                ", role=" + role +
                ", status=" + status +
                '}';
    }
}