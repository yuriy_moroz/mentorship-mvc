package com.epam.mentorship.model;

import java.io.Serializable;

public class AbstractModel implements Serializable {

    private static final long serialVersionUID = -8494305584930443630L;

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
