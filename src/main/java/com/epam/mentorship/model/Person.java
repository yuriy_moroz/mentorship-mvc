package com.epam.mentorship.model;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;
import java.util.Date;

public class Person extends AbstractModel implements Serializable {

    private static final long serialVersionUID = -3621298534283196951L;

    @Size(min=2, max=30)
    private String name;

    @NotEmpty
    @Email
    private String email;

    @NotNull
    private Level level;

    @NotNull
    private String primarySkill;

    @Max(100)
    private Long manager;

    @DateTimeFormat(pattern="MM/dd/yyyy")
    @NotNull
    private Date birthDate;

    public enum Level {
        L1,
        L2,
        L3,
        L4,
        L5
    }

    public Person(Long id, String name, String email, Level level, String primarySkill, Long manager, Date birthDate) {
        setId(id);
        this.name = name;
        this.email = email;
        this.level = level;
        this.primarySkill = primarySkill;
        this.manager = manager;
        this.birthDate = birthDate;
    }

    public Person() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public String getPrimarySkill() {
        return primarySkill;
    }

    public void setPrimarySkill(String primarySkill) {
        this.primarySkill = primarySkill;
    }

    public Long getManager() {
        return manager;
    }

    public void setManager(Long manager) {
        this.manager = manager;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", level=" + level +
                ", primarySkill='" + primarySkill + '\'' +
                ", manager=" + manager +
                ", birthDate=" + birthDate +
                '}';
    }
}
