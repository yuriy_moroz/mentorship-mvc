package com.epam.mentorship.model;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

public class Program extends AbstractModel implements Serializable {

    private static final long serialVersionUID = 4896491808897167231L;

    @Size(min=2, max=30)
    private String name;

    @NotEmpty
    @NotNull
    private String officeLocation;

    @DateTimeFormat(pattern="MM/dd/yyyy")
    @NotNull
    private Date startDate;

    @DateTimeFormat(pattern="MM/dd/yyyy")
    @NotNull
    private Date endDate;

    public Program(Long id, String name, String officeLocation, Date startDate, Date endDate) {
        setId(id);
        this.name = name;
        this.officeLocation = officeLocation;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Program() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOfficeLocation() {
        return officeLocation;
    }

    public void setOfficeLocation(String officeLocation) {
        this.officeLocation = officeLocation;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "Program{" +
                "name='" + name + '\'' +
                ", officeLocation='" + officeLocation + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
