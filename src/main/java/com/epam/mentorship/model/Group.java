package com.epam.mentorship.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

public class Group extends AbstractModel implements Serializable {

    private static final long serialVersionUID = -1217455856648148123L;

    @NotNull
    private Long mentor;
    @NotNull
    private Long mentee;
    @DateTimeFormat(pattern="MM/dd/yyyy")
    @NotNull
    private Date plannedStart;
    @DateTimeFormat(pattern="MM/dd/yyyy")
    @NotNull
    private Date plannedEnd;
    @DateTimeFormat(pattern="MM/dd/yyyy")
    @NotNull
    private Date actualStart;
    @DateTimeFormat(pattern="MM/dd/yyyy")
    @NotNull
    private Date actualEnd;
    @NotNull
    private Status status;

    public Group() {}

    public Group(Long id, Long mentor, Long mentee, Date plannedStart, Date plannedEnd, Date actualStart, Date actualEnd, Status status) {
        setId(id);
        this.mentor = mentor;
        this.mentee = mentee;
        this.plannedStart = plannedStart;
        this.plannedEnd = plannedEnd;
        this.actualStart = actualStart;
        this.actualEnd = actualEnd;
        this.status = status;
    }

    public Long getMentor() {
        return mentor;
    }

    public void setMentor(Long mentor) {
        this.mentor = mentor;
    }

    public Long getMentee() {
        return mentee;
    }

    public void setMentee(Long mentee) {
        this.mentee = mentee;
    }

    public Date getPlannedStart() {
        return plannedStart;
    }

    public void setPlannedStart(Date plannedStart) {
        this.plannedStart = plannedStart;
    }

    public Date getPlannedEnd() {
        return plannedEnd;
    }

    public void setPlannedEnd(Date plannedEnd) {
        this.plannedEnd = plannedEnd;
    }

    public Date getActualStart() {
        return actualStart;
    }

    public void setActualStart(Date actualStart) {
        this.actualStart = actualStart;
    }

    public Date getActualEnd() {
        return actualEnd;
    }

    public void setActualEnd(Date actualEnd) {
        this.actualEnd = actualEnd;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Group{" +
                "mentor=" + mentor +
                ", mentee=" + mentee +
                ", plannedStart=" + plannedStart +
                ", plannedEnd=" + plannedEnd +
                ", actualStart=" + actualStart +
                ", actualEnd=" + actualEnd +
                ", status=" + status +
                '}';
    }
}
