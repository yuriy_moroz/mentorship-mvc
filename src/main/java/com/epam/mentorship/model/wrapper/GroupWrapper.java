package com.epam.mentorship.model.wrapper;

import com.epam.mentorship.model.Group;
import com.epam.mentorship.model.Status;

import java.util.Date;

public class GroupWrapper {

    private Long id;
    private String mentorName;
    private String menteeName;
    private Date plannedStart;
    private Date plannedEnd;
    private Date actualStart;
    private Date actualEnd;
    private Status status;

    public GroupWrapper(Group group, String mentorName, String menteeName) {
        this.id = group.getId();
        this.plannedStart = group.getPlannedStart();
        this.plannedEnd = group.getPlannedEnd();
        this.actualStart = group.getActualStart();
        this.actualEnd = group.getActualEnd();
        this.status = group.getStatus();
        this.menteeName = menteeName;
        this.mentorName = mentorName;
    }

    public Long getId() {
        return id;
    }

    public String getMentorName() {
        return mentorName;
    }

    public String getMenteeName() {
        return menteeName;
    }

    public Date getPlannedStart() {
        return plannedStart;
    }

    public Date getPlannedEnd() {
        return plannedEnd;
    }

    public Date getActualStart() {
        return actualStart;
    }

    public Date getActualEnd() {
        return actualEnd;
    }

    public Status getStatus() {
        return status;
    }
}
