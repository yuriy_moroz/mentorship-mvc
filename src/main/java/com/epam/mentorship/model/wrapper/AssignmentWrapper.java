package com.epam.mentorship.model.wrapper;

import com.epam.mentorship.model.Assignment;

public class AssignmentWrapper {

    private Long id;
    private Long person;
    private String personName;
    private Assignment.Role role;
    private Assignment.Status status;

    public AssignmentWrapper(Assignment assignment, String personName) {
        this.id = assignment.getId();
        this.person = assignment.getPerson();
        this.role = assignment.getRole();
        this.status = assignment.getStatus();
        this.personName = personName;
    }

    public Long getId() {
        return id;
    }

    public Long getPerson() {
        return person;
    }

    public String getPersonName() {
        return personName;
    }

    public Assignment.Role getRole() {
        return role;
    }

    public Assignment.Status getStatus() {
        return status;
    }
}
