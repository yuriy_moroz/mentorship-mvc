package com.epam.mentorship.model;

public enum Status {
    INITIATION,
    IN_PROGRESS,
    FINISHED,
    CANCELED
}
