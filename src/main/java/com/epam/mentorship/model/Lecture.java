package com.epam.mentorship.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

public class Lecture extends AbstractModel implements Serializable {

    private static final long serialVersionUID = -3395480913403610215L;

    @Size(min=5, max=30)
    private String domainArea;

    @Size(min=5, max=30)
    private String topic;

    @NotNull
    private Long lector;

    @NotNull
    @Max(10800000)
    private Long duration;

    @NotNull
    private Status status;

    @DateTimeFormat(pattern = "YYYY/MM/dd hh:mm")
    private Date scheduledTime;

    public Lecture(Long id, String domainArea, String topic, Long lector, Long duration, Status status, Date scheduledTime) {
        setId(id);
        this.domainArea = domainArea;
        this.topic = topic;
        this.lector = lector;
        this.duration = duration;
        this.status = status;
        this.scheduledTime = scheduledTime;
    }

    public Lecture() {
    }

    public String getDomainArea() {
        return domainArea;
    }

    public void setDomainArea(String domainArea) {
        this.domainArea = domainArea;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Long getLector() {
        return lector;
    }

    public void setLector(Long lector) {
        this.lector = lector;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getScheduledTime() {
        return scheduledTime;
    }

    public void setScheduledTime(Date scheduledTime) {
        this.scheduledTime = scheduledTime;
    }

    @Override
    public String toString() {
        return "Lecture{" +
                "domainArea='" + domainArea + '\'' +
                ", topic='" + topic + '\'' +
                ", lector=" + lector +
                ", duration=" + duration +
                ", status=" + status +
                ", scheduledTime=" + scheduledTime +
                '}';
    }
}
