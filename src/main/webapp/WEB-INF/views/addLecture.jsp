<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="springForm"%>
<html>
<style>
.error {
	color: #ff0000;
	font-style: italic;
	font-weight: bold;
}
</style>
<body>
    <c:url var="lectureUrl" value="/lecture"/>
    <div>
        <springForm:form commandName="lecture" action="${lectureUrl}">
        		<table>
        			<tr>
        				<td>Topic:</td>
        				<td><springForm:input path="topic" /></td>
        				<td><springForm:errors path="topic" cssClass="error" /></td>
        			</tr>
        			<tr>
        				<td>Domain Area:</td>
        				<td><springForm:input path="domainArea" /></td>
        				<td><springForm:errors path="domainArea" cssClass="error" /></td>
        			</tr>
                    <tr>
                        <td>Lector:</td>
                        <td>
                            <springForm:select path="lector">
                                    <springForm:option value="" label="Select lector" />
                                    <c:forEach var="lector" items="${lectors}">
                                        <springForm:option value="${lector.id}" label="${lector.name}" />
                                    </c:forEach>
                            </springForm:select></td>
                        <td><springForm:errors path="lector" cssClass="error" /></td>
                    </tr>
        			<tr>
        				<td>Duration:</td>
        				<td><springForm:select path="duration">
        						<springForm:option value="" label="Select level" />
        						<springForm:option value="1800000" label="30" />
        						<springForm:option value="2700000" label="45" />
        						<springForm:option value="3600000" label="60" />
        						<springForm:option value="7200000" label="120" />
        						<springForm:option value="10800000" label="180" />
        					</springForm:select></td>
        				<td><springForm:errors path="duration" cssClass="error" /></td>
        			</tr>
        			<tr>
        				<td>Status:</td>
        				<td><springForm:select path="status">
        						<springForm:option value="" label="Select status" />
        						<springForm:option value="INITIATION" label="initiation" />
        						<springForm:option value="IN_PROGRESS" label="in progress" />
        						<springForm:option value="FINISHED" label="finished" />
        						<springForm:option value="CANCELED" label="canceled" />
        					</springForm:select></td>
        				<td><springForm:errors path="status" cssClass="error" /></td>
        			</tr>
        			<tr>
        				<td>Scheduled Time:</td>
        				<td><springForm:input path="scheduledTime" placeholder="YYYY/MM/dd hh:mm"/></td>
        				<td><springForm:errors path="scheduledTime" cssClass="error" /></td>
        			</tr>
        			<tr>
        				<td colspan="3"><input type="submit" value="Save lecture"></td>
        			</tr>
        		</table>
        	</springForm:form>
    </div>
</body>
</html>