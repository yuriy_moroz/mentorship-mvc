<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="springForm"%>
<html>
<style>
.error {
	color: #ff0000;
	font-style: italic;
	font-weight: bold;
}
</style>
<body>
    <c:url var="groupUrl" value="/group"/>
    <div>
        <springForm:form commandName="group" action="${groupUrl}">
            <table>
                <tr>
                    <td>Mentor:</td>
                    <td>
                        <springForm:select path="mentor">
                                <springForm:option value="" label="Select mentor" />
                                <c:forEach var="person" items="${persons}">
                                    <springForm:option value="${person.id}" label="${person.name}" />
                                </c:forEach>
                        </springForm:select></td>
                    <td><springForm:errors path="mentor" cssClass="error" /></td>
                </tr>
                <tr>
                    <td>Mentee:</td>
                    <td>
                        <springForm:select path="mentee">
                                <springForm:option value="" label="Select mentee" />
                                <c:forEach var="person" items="${persons}">
                                    <springForm:option value="${person.id}" label="${person.name}" />
                                </c:forEach>
                        </springForm:select></td>
                    <td><springForm:errors path="mentee" cssClass="error" /></td>
                </tr>
                <tr>
                    <td>Status:</td>
                    <td><springForm:select path="status">
                            <springForm:option value="" label="Select status" />
                            <springForm:option value="INITIATION" label="initiation" />
                            <springForm:option value="IN_PROGRESS" label="in progress" />
                            <springForm:option value="FINISHED" label="finished" />
                            <springForm:option value="CANCELED" label="canceled" />
                        </springForm:select></td>
                    <td><springForm:errors path="status" cssClass="error" /></td>
                </tr>
                <tr>
                    <td>Planned start:</td>
                    <td><springForm:input path="plannedStart" placeholder="MM/dd/yyyy"/></td>
                    <td><springForm:errors path="plannedStart" cssClass="error" /></td>
                </tr>
                <tr>
                    <td>Planned end:</td>
                    <td><springForm:input path="plannedEnd" placeholder="MM/dd/yyyy"/></td>
                    <td><springForm:errors path="plannedEnd" cssClass="error" /></td>
                </tr>
                <tr>
                    <td>Actual start:</td>
                    <td><springForm:input path="actualStart" placeholder="MM/dd/yyyy"/></td>
                    <td><springForm:errors path="actualStart" cssClass="error" /></td>
                </tr>
                <tr>
                    <td>Actual end:</td>
                    <td><springForm:input path="actualEnd" placeholder="MM/dd/yyyy"/></td>
                    <td><springForm:errors path="actualEnd" cssClass="error" /></td>
                </tr>
                <tr>
                    <td colspan="3"><input type="submit" value="Save lecture"></td>
                </tr>
            </table>
        </springForm:form>
    </div>
</body>
</html>