<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="springForm"%>
<html>
<style>
.error {
	color: #ff0000;
	font-style: italic;
	font-weight: bold;
}

th, td {
    align-self: center;
    padding:5px;
    margin:10px;
}
</style>
<body>
	<h2>List of persons</h2>
	<a href="<c:url value='/person/add' />">Add new person</a>
	<div>
        <table border="1" width="80%">
                <col style="width:25%">
                <col style="width:25%">
                <col style="width:10%">
                <col style="width:20%">
                <col style="width:20%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Level</th>
                    <th>Primary Skill</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            <c:forEach var="person" items="${persons}">
                <tr>
                    <td>${person.name}</td>
                    <td>${person.email}</td>
                    <td>${person.level}</td>
                    <td>${person.primarySkill}</td>
                    <td><a href="<c:url value='/person/${person.id}' />">Details</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
	</div>
</body>
</html>