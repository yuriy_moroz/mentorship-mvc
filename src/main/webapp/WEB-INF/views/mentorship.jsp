<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<body>
    <h3>Mentorship program</h3>
	<p><a href="<c:url value='/person' />">Persons</a>
	<p><a href="<c:url value='/program' />">Programs</a>
	<p><a href="<c:url value='/lecture' />">Lectures</a>
	<p><a href="<c:url value='/assignment' />">Assignments</a>
	<p><a href="<c:url value='/group' />">Groups</a>
</body>
</html>