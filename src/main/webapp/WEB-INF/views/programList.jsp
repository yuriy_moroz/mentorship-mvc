<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="springForm"%>
<html>
<style>
.error {
	color: #ff0000;
	font-style: italic;
	font-weight: bold;
}

th, td {
    align-self: center;
    padding:5px;
    margin:10px;
}
</style>
<body>
	<h2>List of persons</h2>
	<a href="<c:url value='/program/add' />">Add new program</a>
	<div>
        <table border="1" width="80%">
                <col style="width:25%">
                <col style="width:25%">
                <col style="width:10%">
                <col style="width:20%">
                <col style="width:20%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Office Location</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            <c:forEach var="program" items="${programs}">
                <tr>
                    <td>${program.name}</td>
                    <td>${program.officeLocation}</td>
                    <td>${program.startDate}</td>
                    <td>${program.endDate}</td>
                    <td><a href="<c:url value='/program/${program.id}' />">Details</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
	</div>
</body>
</html>