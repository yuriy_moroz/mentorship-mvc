<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="springForm"%>
<html>
<style>
.error {
	color: #ff0000;
	font-style: italic;
	font-weight: bold;
}

th, td {
    align-self: center;
    padding:5px;
    margin:10px;
}
</style>
<body>
	<h2>List of groups</h2>
	<a href="<c:url value='/group/add' />">Add new group</a>
	<div>
        <table border="1" width="80%">
                <col style="width:15%">
                <col style="width:15%">
                <col style="width:15%">
                <col style="width:15%">
                <col style="width:15%">
                <col style="width:15%">
                <col style="width:5%">
                <col style="width:5%">
            <thead>
                <tr>
                    <th>Mentor</th>
                    <th>Mentee</th>
                    <th>Planned start</th>
                    <th>Planned end</th>
                    <th>Actual start</th>
                    <th>Actual end</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            <c:forEach var="group" items="${groups}">
                <tr>
                    <td>${group.mentorName}</td>
                    <td>${group.menteeName}</td>
                    <td>${group.plannedStart}</td>
                    <td>${group.plannedEnd}</td>
                    <td>${group.actualStart}</td>
                    <td>${group.actualEnd}</td>
                    <td>${group.status}</td>
                    <td><a href="<c:url value='/group/${group.id}' />">Details</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
	</div>
</body>
</html>