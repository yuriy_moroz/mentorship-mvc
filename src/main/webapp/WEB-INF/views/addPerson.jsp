<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="springForm"%>
<html>
<style>
.error {
	color: #ff0000;
	font-style: italic;
	font-weight: bold;
}
</style>
<body>
    <c:url var="personUrl" value="/person"/>
    <div>
        <springForm:form commandName="person" action="${personUrl}">
        		<table>
        			<tr>
        				<td>Name:</td>
        				<td><springForm:input path="name" /></td>
        				<td><springForm:errors path="name" cssClass="error" /></td>
        			</tr>
        			<tr>
        				<td>Email:</td>
        				<td><springForm:input path="email" /></td>
        				<td><springForm:errors path="email" cssClass="error" /></td>
        			</tr>
        			<tr>
        				<td>Level:</td>
        				<td><springForm:select path="level">
        						<springForm:option value="" label="Select level" />
        						<springForm:option value="L1" label="L1" />
        						<springForm:option value="L2" label="L2" />
        						<springForm:option value="L3" label="L3" />
        						<springForm:option value="L4" label="L4" />
        						<springForm:option value="L5" label="L5" />
        					</springForm:select></td>
        				<td><springForm:errors path="level" cssClass="error" /></td>
        			</tr>
        			<tr>
        				<td>Primary Skill:</td>
        				<td><springForm:input path="primarySkill" /></td>
        				<td><springForm:errors path="primarySkill" cssClass="error" /></td>
        			</tr>
        			<tr>
        				<td>Birth date:</td>
        				<td><springForm:input path="birthDate" placeholder="MM/dd/yyyy"/></td>
        				<td><springForm:errors path="birthDate" cssClass="error" /></td>
        			</tr>
                    <tr>
                        <td>Manager:</td>
                        <td>
                            <springForm:select path="manager">
                                    <springForm:option value="" label="Select manager" />
                                    <c:forEach var="manager" items="${managers}">
                                        <springForm:option value="${manager.id}" label="${manager.name}" />
                                    </c:forEach>
                            </springForm:select></td>
                        <td><springForm:errors path="manager" cssClass="error" /></td>
                    </tr>
        			<tr>
        				<td colspan="3"><input type="submit" value="Save person"></td>
        			</tr>
        		</table>
        	</springForm:form>
    </div>
</body>
</html>