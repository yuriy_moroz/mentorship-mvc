<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="springForm"%>
<html>
<style>
.error {
	color: #ff0000;
	font-style: italic;
	font-weight: bold;
}
</style>
<body>
    <div>
        <springForm:form commandName="assignment" action="">
            <table>
                <tr>
                    <td>Person:</td>
                    <td>
                        <springForm:select path="person">
                                <springForm:option value="${assignment.person}" label="${assignment.personName}" />
                                <c:forEach var="person" items="${persons}">
                                    <springForm:option value="${person.id}" label="${person.name}" />
                                </c:forEach>
                        </springForm:select></td>
                    <td><springForm:errors path="person" cssClass="error" /></td>
                </tr>
                <tr>
                    <td>Role:</td>
                    <td><springForm:select path="role">
                            <springForm:option value="" label="Select role" />
                            <springForm:option value="MENTOR" label="mentor" />
                            <springForm:option value="MENTEE" label="mentee" />
                            <springForm:option value="CURATOR" label="curator" />
                            <springForm:option value="LECTOR" label="lector" />
                        </springForm:select></td>
                    <td><springForm:errors path="role" cssClass="error" /></td>
                </tr>
                <tr>
                    <td>Status:</td>
                    <td><springForm:select path="status">
                            <springForm:option value="" label="Select status" />
                            <springForm:option value="PROPOSED" label="proposed" />
                            <springForm:option value="APPROVED_RM" label="approved rm" />
                            <springForm:option value="CONFIRMED_CDP" label="confirmed cdp" />
                            <springForm:option value="ON_HOLD" label="on hold" />
                        </springForm:select></td>
                    <td><springForm:errors path="status" cssClass="error" /></td>
                </tr>
                <tr>
                    <td colspan="3"><input type="submit" value="Update assignment"></td>
                </tr>
            </table>
        </springForm:form>
    </div>
</body>
</html>