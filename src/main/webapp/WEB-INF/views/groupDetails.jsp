<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="springForm"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html>
<style>
.error {
	color: #ff0000;
	font-style: italic;
	font-weight: bold;
}
</style>
<body>
    <div>
        <springForm:form commandName="group" action="">
            <table>
                <tr>
                    <td>Mentor:</td>
                    <td>
                        <spring:nestedPath path="group">
                            <input type="hidden" name="mentor" value="${mentor.id}" />
                        </spring:nestedPath>${mentor.name}
                    </td>
                </tr>
                <tr>
                    <td>Mentor:</td>
                    <td>
                        <spring:nestedPath path="group">
                            <input type="hidden" name="mentee" value="${mentee.id}" />
                        </spring:nestedPath>${mentor.name}
                    </td>
                </tr>
                <tr>
                    <td>Status:</td>
                    <td><springForm:select path="status">
                            <springForm:option value="" label="Select status" />
                            <springForm:option value="INITIATION" label="initiation" />
                            <springForm:option value="IN_PROGRESS" label="in progress" />
                            <springForm:option value="FINISHED" label="finished" />
                            <springForm:option value="CANCELED" label="canceled" />
                        </springForm:select></td>
                    <td><springForm:errors path="status" cssClass="error" /></td>
                </tr>
                <tr>
                    <td>Planned start:</td>
                    <td><springForm:input path="plannedStart" placeholder="MM/dd/yyyy"/></td>
                    <td><springForm:errors path="plannedStart" cssClass="error" /></td>
                </tr>
                <tr>
                    <td>Planned end:</td>
                    <td><springForm:input path="plannedEnd" placeholder="MM/dd/yyyy"/></td>
                    <td><springForm:errors path="plannedEnd" cssClass="error" /></td>
                </tr>
                <tr>
                    <td>Actual start:</td>
                    <td><springForm:input path="actualStart" placeholder="MM/dd/yyyy"/></td>
                    <td><springForm:errors path="actualStart" cssClass="error" /></td>
                </tr>
                <tr>
                    <td>Actual end:</td>
                    <td><springForm:input path="actualEnd" placeholder="MM/dd/yyyy"/></td>
                    <td><springForm:errors path="actualEnd" cssClass="error" /></td>
                </tr>
                <tr>
                    <td colspan="3"><input type="submit" value="Update lecture"></td>
                </tr>
            </table>
        </springForm:form>
    </div>
</body>
</html>