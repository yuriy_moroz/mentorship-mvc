<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="springForm"%>
<html>
<style>
.error {
	color: #ff0000;
	font-style: italic;
	font-weight: bold;
}

th, td {
    align-self: center;
    padding:5px;
    margin:10px;
}
</style>
<body>
	<h2>List of assignments</h2>
	<a href="<c:url value='/assignment/add' />">Add new assignment</a>
	<div>
        <table border="1" width="50%">
                <col style="width:40%">
                <col style="width:30%">
                <col style="width:30%">
            <thead>
                <tr>
                    <th>Person</th>
                    <th>Status</th>
                    <th>Role</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="assignment" items="${assignments}">
                    <c:set var="assigmentStaus" value="${fn:replace(assignment.status, '_', ' ')}" />
                    <tr>
                        <td>${assignment.personName}</td>
                        <td>${fn:toLowerCase(assigmentStaus)}</td>
                        <td>${fn:toLowerCase(assignment.role)}</td>
                        <td><a href="<c:url value='/assignment/${assignment.id}' />">Details</a></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
	</div>
</body>
</html>