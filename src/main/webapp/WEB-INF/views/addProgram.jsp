<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="springForm"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Customer Save Page</title>
<style>
.error {
	color: #ff0000;
	font-style: italic;
	font-weight: bold;
}
</style>
</head>
<body>
    <h2>Program details</h2>
    <c:url var="programUrl" value="/program"/>
	<springForm:form commandName="program" action="${programUrl}">
		<table>
			<tr>
				<td>Name:</td>
				<td><springForm:input path="name" /></td>
				<td><springForm:errors path="name" cssClass="error" /></td>
			</tr>
			<tr>
				<td>Office location:</td>
				<td><springForm:input path="officeLocation" /></td>
				<td><springForm:errors path="officeLocation" cssClass="error" /></td>
			</tr>
			<tr>
				<td>Start Date:</td>
				<td><springForm:input path="startDate" placeholder="MM/dd/yyyy"/></td>
				<td><springForm:errors path="startDate" cssClass="error" /></td>
			</tr>
			<tr>
				<td>End Date:</td>
				<td><springForm:input path="endDate" placeholder="MM/dd/yyyy"/></td>
				<td><springForm:errors path="endDate" cssClass="error" /></td>
			</tr>
			<tr>
				<td colspan="3"><input type="submit" value="Update program"></td>
			</tr>
		</table>
	</springForm:form>
</body>
</html>