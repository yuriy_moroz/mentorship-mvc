<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="springForm"%>
<html>
<style>
.error {
	color: #ff0000;
	font-style: italic;
	font-weight: bold;
}

th, td {
    align-self: center;
    padding:5px;
    margin:10px;
}
</style>
<body>
	<h2>List of persons</h2>
	<a href="<c:url value='/lecture/add' />">Add new lecture</a>
	<div>
        <table border="1" width="80%">
                <col style="width:25%">
                <col style="width:25%">
                <col style="width:10%">
                <col style="width:20%">
                <col style="width:20%">
            <thead>
                <tr>
                    <th>Topic</th>
                    <th>Duration</th>
                    <th>Scheduled Time</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            <c:forEach var="lecture" items="${lectures}">
                <tr>
                    <td>${lecture.topic}</td>
                    <td><fmt:formatNumber value='${lecture.duration/60000}' pattern='#'/> min</td>
                    <td>${lecture.scheduledTime}</td>
                    <td><a href="<c:url value='/lecture/${lecture.id}' />">Details</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
	</div>
</body>
</html>